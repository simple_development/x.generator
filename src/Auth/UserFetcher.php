<?php
declare(strict_types=1);

namespace App\Auth;


use Doctrine\DBAL\Connection;

/**
 * Class UserFetcher
 * @package App\Auth
 */
class UserFetcher
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * UserFetcher constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $token
     * @return UserIdentity
     */
    public function findByToken(string $token): UserIdentity
    {
        $stmt = $this
                        ->connection
                        ->createQueryBuilder()
                        ->select('u.id, u.email, u.lvl')
                        ->from('users', 'u')
                        ->innerJoin('u', 'tokens', 't', 't.users_id = u.id')
                        ->where('t.token = :token')
                        ->setParameter('token', $token)
                        ->execute();

        $stmt->setFetchMode(\PDO::FETCH_CLASS, UserIdentity::class);

        $user = $stmt->fetch();

        return $user;
    }
}
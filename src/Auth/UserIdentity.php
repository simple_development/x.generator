<?php
declare(strict_types=1);

namespace App\Auth;

/**
 * Class UserIdentity
 * @package App\Auth
 */
class UserIdentity
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $email;
    /**
     * @var int
     */
    public $lvl;
}
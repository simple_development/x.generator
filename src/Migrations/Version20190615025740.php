<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190615025740 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE workout_values (id INT AUTO_INCREMENT NOT NULL, attributes_id INT DEFAULT NULL, practices_id INT DEFAULT NULL, value INT NOT NULL, INDEX IDX_20245328BAAF4009 (attributes_id), INDEX IDX_20245328AB167E43 (practices_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE workout_programs (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(30) NOT NULL, price INT UNSIGNED NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE workout_routines (id INT AUTO_INCREMENT NOT NULL, programs_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, INDEX IDX_AC84A38479AEC3C (programs_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE workout_attributes (id INT AUTO_INCREMENT NOT NULL, routines_id INT DEFAULT NULL, value_name VARCHAR(100) NOT NULL, value_min INT NOT NULL, value_max INT NOT NULL, value_append INT NOT NULL, value_order INT NOT NULL, INDEX IDX_5DBFD1FAE28390A2 (routines_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE workout_practices (id INT AUTO_INCREMENT NOT NULL, users_id INT DEFAULT NULL, success TINYINT(1) NOT NULL, status SMALLINT NOT NULL, created DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', INDEX IDX_A8CF28FC67B3B43D (users_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(50) NOT NULL, password VARCHAR(30) NOT NULL, lvl INT DEFAULT 1 NOT NULL, confirmed TINYINT(1) DEFAULT \'0\' NOT NULL, UNIQUE INDEX UNIQ_1483A5E9E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE workout_values ADD CONSTRAINT FK_20245328BAAF4009 FOREIGN KEY (attributes_id) REFERENCES workout_attributes (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE workout_values ADD CONSTRAINT FK_20245328AB167E43 FOREIGN KEY (practices_id) REFERENCES workout_practices (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE workout_routines ADD CONSTRAINT FK_AC84A38479AEC3C FOREIGN KEY (programs_id) REFERENCES workout_programs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE workout_attributes ADD CONSTRAINT FK_5DBFD1FAE28390A2 FOREIGN KEY (routines_id) REFERENCES workout_routines (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE workout_practices ADD CONSTRAINT FK_A8CF28FC67B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workout_routines DROP FOREIGN KEY FK_AC84A38479AEC3C');
        $this->addSql('ALTER TABLE workout_attributes DROP FOREIGN KEY FK_5DBFD1FAE28390A2');
        $this->addSql('ALTER TABLE workout_values DROP FOREIGN KEY FK_20245328BAAF4009');
        $this->addSql('ALTER TABLE workout_values DROP FOREIGN KEY FK_20245328AB167E43');
        $this->addSql('ALTER TABLE workout_practices DROP FOREIGN KEY FK_A8CF28FC67B3B43D');
        $this->addSql('DROP TABLE workout_values');
        $this->addSql('DROP TABLE workout_programs');
        $this->addSql('DROP TABLE workout_routines');
        $this->addSql('DROP TABLE workout_attributes');
        $this->addSql('DROP TABLE workout_practices');
        $this->addSql('DROP TABLE users');
    }
}

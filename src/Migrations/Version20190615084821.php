<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190615084821 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workout_values ADD routines_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE workout_values ADD CONSTRAINT FK_20245328E28390A2 FOREIGN KEY (routines_id) REFERENCES workout_routines (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_20245328E28390A2 ON workout_values (routines_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE workout_values DROP FOREIGN KEY FK_20245328E28390A2');
        $this->addSql('DROP INDEX IDX_20245328E28390A2 ON workout_values');
        $this->addSql('ALTER TABLE workout_values DROP routines_id');
    }
}

<?php
declare(strict_types=1);

namespace App\EventSubscriber;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RequestSubscriber implements EventSubscriberInterface
{
    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['parse', 1],
                ['cors', 2],
            ]
        ];
    }

    public function parse(RequestEvent $event): void
    {
        $request = $event->getRequest();

        if(0 === strpos($request->headers->get('Content-Type') ?: "", "application/json")){

            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : []);

        }
    }

    public function cors(RequestEvent $event)
    {
        $response = $event->getResponse();
    }

}

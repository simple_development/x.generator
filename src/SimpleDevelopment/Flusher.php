<?php
declare(strict_types=1);

namespace App\SimpleDevelopment;


use Doctrine\Common\Persistence\ObjectManager;

class Flusher
{
    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    public function flush(): void
    {
        $this->manager->flush();
    }
}
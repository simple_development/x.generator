<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Users\Entity\User;


interface UserRepositoryInterface
{

    /**
     * @param User $user
     */
    public function add(User $user): void;

    /**
     * @param string $email
     * @return User|null
     */
    public function findByEmail(string $email): ?User;

    /**
     * @param int $id
     * @return User
     */
    public function get(int $id): User;

    /**
     * @param User $user
     */
    public function update(User $user): void;
}
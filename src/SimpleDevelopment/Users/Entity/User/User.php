<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Users\Entity\User;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class User
 * @package App\SimpleDevelopment\Users\Entity\User
 * @ORM\Entity()
 * @ORM\Table(name="users")
 */
class User
{
    /**
     * @Groups({"rest"})
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var int|null
     */
    private $id;

    /**
     * @Groups({"rest"})
     * @ORM\Column(type="string", length=50, unique=true)
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=30)
     * @var string
     */
    private $password;

    /**
     * @Groups({"rest"})
     * @ORM\Column(type="integer", length=11, options={"default": 1})
     * @var int
     */
    private $lvl;

    /**
     * @Groups({"rest"})
     * @ORM\Column(type="boolean", nullable=false, options={"default": false})
     * @var bool
     */
    private $confirmed;

    /**
     * @ORM\OneToMany(targetEntity="App\SimpleDevelopment\Users\Entity\User\Token", mappedBy="user", cascade={"persist"})
     * @var Token[]|ArrayCollection
     */
    private $tokens;

    public function __construct(string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
        $this->lvl = 1;
        $this->confirmed = false;
        $this->tokens = new ArrayCollection();
    }


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function passwordVerify(string $password): bool
    {
        return $this->password === $password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getLvl(): int
    {
        return $this->lvl;
    }

    public function lvlUp(): void
    {
        $this->setLvl($this->getLvl() + 1);
    }

    public function lvlDown(): void
    {
        if($this->lvl > 1){
            $this->setLvl($this->getLvl() - 1);
        }
    }

    /**
     * @param int $lvl
     */
    public function setLvl(int $lvl): void
    {
        if($lvl <= 0){
            throw new \DomainException("Уровень не может быть меньше нуля");
        }

        $this->lvl = $lvl;
    }

    /**
     * @return bool
     */
    public function isConfirmed(): bool
    {
        return $this->confirmed;
    }


    public function confirm(): void
    {
        $this->confirmed = true;
    }


    public function joinToken(Token $token): void
    {
        if($this->tokens->contains($token)){
            return;
        }

        $this->tokens->add($token);
    }

}
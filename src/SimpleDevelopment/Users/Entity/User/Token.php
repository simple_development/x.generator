<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Users\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Token
 * @package App\SimpleDevelopment\Users\Entity\User
 * @ORM\Entity()
 * @ORM\Table(name="tokens")
 */
class Token
{
    /**
     * @ORM\Column(type="integer", length=111)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var int|null
     */
    private $id;

    /**
     * @Groups({"rest"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $token;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateInterval
     */
    private $expire;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeImmutable
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity="App\SimpleDevelopment\Users\Entity\User\User", inversedBy="tokens")
     * @ORM\JoinColumn(name="users_id", referencedColumnName="id", onDelete="CASCADE")
     * @var User
     */
    private $user;

    public function __construct(string $token, User $user, \DateTime $created, \DateTime $expire)
    {
        $this->token = $token;
        $this->created = $created;
        $this->expire = $expire;
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    public function isAlive(): bool
    {
        return time() < $this->expire;
    }
}
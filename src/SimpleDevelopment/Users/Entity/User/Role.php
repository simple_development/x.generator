<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Users\Entity\User;


class Role
{
    public const CUSTOMER = 1;
    public const ADMIN = 2;

    private $role;

    private function __construct(int $role)
    {
        $this->role = $role;
    }

    public static function customer(): self
    {
        return new self(self::CUSTOMER);
    }

    public static function admin(): self
    {
        return new self(self::ADMIN);
    }

    public function __toString()
    {
        return (string) $this->role;
    }
}
<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Users\UseCase\LogIn\Request;


use App\SimpleDevelopment\Flusher;
use App\SimpleDevelopment\Users\Entity\User\Token;
use App\SimpleDevelopment\Users\Entity\User\UserRepositoryInterface;

class Handler
{
    /**
     * @var UserRepositoryInterface
     */
    private $users;
    /**
     * @var Flusher
     */
    private $flusher;

    public function __construct(UserRepositoryInterface $users, Flusher $flusher)
    {

        $this->users = $users;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): Token
    {
        if(!$user = $this->users->findByEmail($command->email)){
            throw new \DomainException("Пользователь не найден");
        }

        if(!$user->passwordVerify($command->password)){
            throw new \DomainException("Неверный пароль");
        }

        $hash = base64_encode(uniqid());

        $created = new \DateTime();
        $expire = new \DateTime();
        $expire->add(new \DateInterval("P30D"));

        $token = new Token($hash, $user, $created, $expire);
        $user->joinToken($token);

        $this->users->update($user);

        $this->flusher->flush();

        return $token;

    }
}
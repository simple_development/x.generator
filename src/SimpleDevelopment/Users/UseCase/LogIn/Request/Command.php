<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Users\UseCase\LogIn\Request;


class Command
{
    public $email;
    public $password;
}
<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Users\UseCase\SignUp\Request;


use App\SimpleDevelopment\Flusher;
use App\SimpleDevelopment\Users\Entity\User\User;
use App\SimpleDevelopment\Users\Entity\User\UserRepositoryInterface;

class Handler
{
    /**
     * @var UserRepositoryInterface
     */
    private $users;
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * Handler constructor.
     * @param UserRepositoryInterface $users
     * @param Flusher $flusher
     */
    public function __construct(UserRepositoryInterface $users, Flusher $flusher)
    {
        $this->users = $users;
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     * @return User
     */
    public function handle(Command $command): User
    {
        if($this->users->findByEmail($command->email)){
            throw new \DomainException("Пользователь с таким email уже существует");
        }

        $user = new User($command->email, $command->password);

        $this->users->add($user);

        $this->flusher->flush();

        return $user;
    }
}
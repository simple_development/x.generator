<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Users\Repository;


use App\SimpleDevelopment\Users\Entity\User\User;
use App\SimpleDevelopment\Users\Entity\User\UserRepositoryInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserRepository implements UserRepositoryInterface
{

    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->repository = $manager->getRepository(User::class);
    }

    /**
     * @param User $user
     */
    public function add(User $user): void
    {
        $this->manager->persist($user);
    }

    /**
     * @param string $email
     * @return User|null
     */
    public function findByEmail(string $email): ?User
    {
        return $this->repository->findOneBy(['email' => $email]);
    }

    /**
     * @param int $id
     * @return User
     */
    public function get(int $id): User
    {
        if(!$user = $this->repository->findOneBy(['id' => $id])){
            throw new \DomainException("Пользователь не найден");
        }

        return $user;
    }

    /**
     * @param User $user
     */
    public function update(User $user): void
    {
        $this->manager->persist($user);
    }
}
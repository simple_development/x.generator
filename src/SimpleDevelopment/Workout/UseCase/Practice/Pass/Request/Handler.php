<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Practice\Pass\Request;


use App\SimpleDevelopment\Flusher;
use App\SimpleDevelopment\Users\Entity\User\UserRepositoryInterface;
use App\SimpleDevelopment\Workout\Entity\Practice\Practice;
use App\SimpleDevelopment\Workout\Entity\Practice\PracticeRepositoryInterface;
use App\SimpleDevelopment\Workout\Entity\Practice\Status;

/**
 * Class Handler
 * @package App\SimpleDevelopment\Workout\UseCase\Practice\Pass\Request
 */
class Handler
{

    /**
     * @var PracticeRepositoryInterface
     */
    private $practices;
    /**
     * @var Flusher
     */
    private $flusher;
    /**
     * @var UserRepositoryInterface
     */
    private $users;

    /**
     * Handler constructor.
     * @param PracticeRepositoryInterface $practices
     * @param UserRepositoryInterface $users
     * @param Flusher $flusher
     */
    public function __construct(
        PracticeRepositoryInterface $practices,
        UserRepositoryInterface $users,
        Flusher $flusher)
    {
        $this->practices = $practices;
        $this->flusher = $flusher;
        $this->users = $users;
    }

    /**
     * @param Command $command
     * @return Practice
     */
    public function handle(Command $command): Practice
    {
        $practice = $this->practices->get($command->practiceId);
        $user = $this->users->get($command->userId);

        $practice->setSuccess(true);
        $practice->setStatus(Status::PASSED);
        $user->lvlUp();

        $this->practices->update($practice);
        $this->users->update($user);

        $this->flusher->flush();

        return $practice;
    }
}
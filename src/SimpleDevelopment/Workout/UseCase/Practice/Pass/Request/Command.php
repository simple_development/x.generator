<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Practice\Pass\Request;


class Command
{
    public $practiceId;

    public $userId;
}
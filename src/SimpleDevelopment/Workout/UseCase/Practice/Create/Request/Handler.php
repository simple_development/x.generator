<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Practice\Create\Request;


use App\SimpleDevelopment\Flusher;
use App\SimpleDevelopment\Users\Entity\User\UserRepositoryInterface;
use App\SimpleDevelopment\Workout\Entity\Practice\Practice;
use App\SimpleDevelopment\Workout\Entity\Practice\PracticeRepositoryInterface;
use App\SimpleDevelopment\Workout\Entity\Practice\Value;
use App\SimpleDevelopment\Workout\Entity\Program\ProgramRepositoryInterface;
use App\SimpleDevelopment\Workout\Entity\Routine\Attribute;
use App\SimpleDevelopment\Workout\Entity\Routine\RoutineRepositoryInterface;

class Handler
{
    /**
     * @var PracticeRepositoryInterface
     */
    private $practices;
    /**
     * @var UserRepositoryInterface
     */
    private $users;
    /**
     * @var ProgramRepositoryInterface
     */
    private $programs;
    /**
     * @var RoutineRepositoryInterface
     */
    private $routines;
    /**
     * @var Flusher
     */
    private $flusher;

    public function __construct(
        PracticeRepositoryInterface $practices,
        UserRepositoryInterface $users,
        ProgramRepositoryInterface $programs,
        RoutineRepositoryInterface $routines,
        Flusher $flusher
    )
    {
        $this->practices = $practices;
        $this->users = $users;
        $this->programs = $programs;
        $this->routines = $routines;
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     * @return Practice
     * @throws \Exception
     */
    public function handle(Command $command): Practice
    {
        $user = $this->users->get($command->userId);
        $program = $this->programs->getById($command->programId);

        $routines = $this->routines->random($program, 5);


        $practice = new Practice($user, new \DateTimeImmutable());


        foreach ($routines as $rKey => $routine){

            /** @var Attribute[] $attributes */
            $attributes = $routine->getAttributes();
            usort($attributes, function($a, $b) {
                /**
                 * @var Attribute $a
                 * @var Attribute $b
                 */
                return ($a->getOrder() < $b->getOrder()) ? -1 : 1;
            });

            $skipAppend = false;

            foreach ($attributes as $attribute){

                $lastValue = $this->practices->findOneValueByAttribute($attribute);

                if($lastValue){
                    if(!$skipAppend && $lastValue->getPractice()->isSuccess()){
                        if($lastValue->getValue() < $attribute->getMax()){
                            $value = new Value($attribute, $routine,$lastValue->getValue() + $lastValue->getAttribute()->getAppend());
                            $skipAppend = true;
                        }else{
                            $value = new Value($attribute, $routine, $lastValue->getValue());
                        }
                    }else{
                        $value = new Value($attribute, $routine, $lastValue->getValue());
                    }
                }else{
                    $value = new Value($attribute, $routine, $attribute->getMin());
                }

                $practice->addValue($value);
            }

        }


        $this->practices->add($practice);

        $this->flusher->flush();

        return $practice;

    }
}
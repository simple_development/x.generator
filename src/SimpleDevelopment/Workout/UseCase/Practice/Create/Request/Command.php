<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Practice\Create\Request;


class Command
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var
     */
    public $programId;
}
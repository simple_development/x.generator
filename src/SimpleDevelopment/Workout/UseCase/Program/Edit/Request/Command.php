<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Program\Edit\Request;


class Command
{
    public $programId;
    public $title;
    public $price;
}
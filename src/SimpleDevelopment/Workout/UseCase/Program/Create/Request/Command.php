<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Program\Create\Request;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank(message="Заголовок не может быть пустым")
     * @Assert\NotNull(message="Заголовок не может быть пустым")
     * @Assert\Type(type="string", message="Заголовок должен быть строкой")
     * @Assert\Length(min="3", minMessage="Слишком короткий заголовок")
     * @var string
     */
    public $title;

    /**
     * @Assert\NotNull(message="Цена не может быть пустой")
     * @Assert\NotBlank(message="Цена не может быть пустой")
     * @Assert\Type(type="int", message="Цена целое число")
     * @var int
     */
    public $price;
}
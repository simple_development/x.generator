<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Program\Create\Request;


use App\SimpleDevelopment\Flusher;
use App\SimpleDevelopment\Workout\Entity\Program\Price;
use App\SimpleDevelopment\Workout\Entity\Program\Program;
use App\SimpleDevelopment\Workout\Entity\Program\ProgramRepositoryInterface;
use App\SimpleDevelopment\Workout\Entity\Program\Title;

class Handler
{

    /**
     * @var ProgramRepositoryInterface
     */
    private $programs;
    /**
     * @var Flusher
     */
    private $flusher;

    public function __construct(ProgramRepositoryInterface $programs, Flusher $flusher)
    {
        $this->programs = $programs;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): Program
    {
        $program = new Program(new Title($command->title), new Price($command->price));

        $this->programs->add($program);

        $this->flusher->flush();

        return $program;
    }
}
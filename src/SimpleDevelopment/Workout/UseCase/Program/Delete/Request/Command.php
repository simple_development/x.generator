<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Program\Delete\Request;


class Command
{
    public $programId;
}
<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Program\Delete\Request;


use App\SimpleDevelopment\Flusher;
use App\SimpleDevelopment\Workout\Entity\Program\ProgramRepositoryInterface;

class Handler
{
    /**
     * @var ProgramRepositoryInterface
     */
    private $programs;
    /**
     * @var Flusher
     */
    private $flusher;

    public function __construct(ProgramRepositoryInterface $programs, Flusher $flusher)
    {
        $this->programs = $programs;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): void
    {
        $this->programs->removeById($command->programId);

        $this->flusher->flush();

        return;
    }
}
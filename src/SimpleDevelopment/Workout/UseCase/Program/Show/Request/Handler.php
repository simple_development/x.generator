<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Program\Show\Request;


use App\SimpleDevelopment\Workout\Entity\Program\Program;
use App\SimpleDevelopment\Workout\Entity\Program\ProgramRepositoryInterface;

/**
 * Class Handler
 * @package App\SimpleDevelopment\SimpleDevelopment\UseCase\Program\Show\Request
 */
class Handler
{
    /**
     * @var ProgramRepositoryInterface
     */
    private $programs;

    public function __construct(ProgramRepositoryInterface $programs)
    {
        $this->programs = $programs;
    }

    /**
     * @param Command|null $command
     * @return Program[]
     */
    public function handle(Command $command = null): array
    {
        $programs = $this->programs->findAll();

        return $programs;
    }
}
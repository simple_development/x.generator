<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Routine\Create\Request;

/**
 * Class Attribute
 * @package App\SimpleDevelopment\Workout\UseCase\Routine\Create\Request
 */
class Value
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    public $min;
    /**
     * @var int
     */
    public $max;
    /**
     * @var int
     */
    public $append;
    /**
     * @var int
     */
    public $order;
    /**
     * @var int
     */
    public $value;


}
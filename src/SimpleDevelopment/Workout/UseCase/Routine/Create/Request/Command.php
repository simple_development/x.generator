<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Routine\Create\Request;


/**
 * Class Command
 * @package App\SimpleDevelopment\Workout\UseCase\Routine\Create\Request
 */
class Command
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $programId;

    /**
     * @var Value[]
     */
    public $values = [];
}
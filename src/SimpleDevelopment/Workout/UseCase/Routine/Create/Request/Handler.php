<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Routine\Create\Request;


use App\SimpleDevelopment\Flusher;
use App\SimpleDevelopment\Workout\Entity\Category\AttributeRepositoryInterface;
use App\SimpleDevelopment\Workout\Entity\Program\ProgramRepositoryInterface;
use App\SimpleDevelopment\Workout\Entity\Routine\Routine;
use App\SimpleDevelopment\Workout\Entity\Routine\RoutineRepositoryInterface;
use App\SimpleDevelopment\Workout\Entity\Routine\Attribute;

class Handler
{
    /**
     * @var AttributeRepositoryInterface
     */
    private $attributes;
    /**
     * @var RoutineRepositoryInterface
     */
    private $routines;
    /**
     * @var Flusher
     */
    private $flusher;
    /**
     * @var ProgramRepositoryInterface
     */
    private $programs;

    public function __construct(
        ProgramRepositoryInterface $programs,
        RoutineRepositoryInterface $routines,
        Flusher $flusher
    )
    {
        $this->routines = $routines;
        $this->flusher = $flusher;
        $this->programs = $programs;
    }


    public function handle(Command $command): Routine
    {
        $program = $this->programs->getById($command->programId);

        $routine = new Routine($command->name, $program);

        foreach ($command->values as $v){
            $value = new Attribute($v->name, $v->min, $v->max, $v->append, $v->order);
            $routine->addValue($value);
        }

        $this->routines->add($routine);

        $this->flusher->flush();

        return $routine;
    }

}
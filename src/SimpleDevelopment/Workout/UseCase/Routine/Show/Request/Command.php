<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Routine\Show\Request;


/**
 * Class Command
 * @package App\SimpleDevelopment\Workout\UseCase\Routine\Show\Request
 */
class Command
{
    /**
     * @var int
     */
    public $programId;

    /**
     * @var int
     */
    public $limit = 15;
    /**
     * @var int
     */
    public $offset = 0;
}
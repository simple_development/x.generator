<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\UseCase\Routine\Show\Request;


use App\SimpleDevelopment\Workout\Entity\Routine\RoutineRepositoryInterface;

class Handler
{
    /**
     * @var RoutineRepositoryInterface
     */
    private $routines;

    /**
     * Handler constructor.
     * @param RoutineRepositoryInterface $routines
     */
    public function __construct(
        RoutineRepositoryInterface $routines
    )
    {
        $this->routines = $routines;
    }

    /**
     * @param Command $command
     * @return array
     */
    public function handle(Command $command): array
    {
        $routines = $this->routines->findByProgramId($command->programId, $command->limit, $command->offset);

        return $routines;
    }
}
<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Repository;


use App\SimpleDevelopment\Workout\Entity\Program\Program;
use App\SimpleDevelopment\Workout\Entity\Routine\Routine;
use App\SimpleDevelopment\Workout\Entity\Routine\RoutineRepositoryInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class RoutineRepository
 * @package App\SimpleDevelopment\Workout\Repository
 */
class RoutineRepository implements RoutineRepositoryInterface
{

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $repository;

    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * RoutineRepository constructor.
     * @param ObjectManager $manager
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(ObjectManager $manager, EntityManagerInterface $entityManager)
    {
        $this->manager = $manager;
        $this->repository = $manager->getRepository(Routine::class);
        $this->em = $entityManager;
    }

    /**
     * @param Routine $routine
     */
    public function add(Routine $routine): void
    {
        $this->manager->persist($routine);

        return;
    }


    /**
     * @param int $id
     * @param int $limit
     * @param int $offset
     * @return Routine[]
     */
    public function findByProgramId(int $id, int $limit, int $offset): array
    {
        return $this->repository->findBy(['program' => $id], [], $limit, $offset);
    }

    /**
     * @param Program $program
     * @param int $count
     * @return Routine[]
     */
    public function random(Program $program, int $count): array
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb
                    ->select('r')
                    ->from(Routine::class, "r")
                    ->orderBy('RAND()')
                    ->where('r.program = :program')
                    ->setParameter('program', $program)
                    ->setMaxResults($count)
                    ->getQuery();

        return $query->getResult();
    }
}
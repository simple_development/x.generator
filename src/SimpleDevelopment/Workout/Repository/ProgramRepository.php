<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Repository;


use App\SimpleDevelopment\Workout\Entity\Program\Program;
use App\SimpleDevelopment\Workout\Entity\Program\ProgramRepositoryInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;

/**
 * Class ProgramRepository
 * @package App\SimpleDevelopment\Workout\Repository
 */
class ProgramRepository implements ProgramRepositoryInterface
{

    /**
     * @var ObjectRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->repository = $manager->getRepository(Program::class);
        $this->manager = $manager;
    }


    /**
     * @param Program $program
     */
    public function add(Program $program): void
    {
        $this->manager->persist($program);
    }

    /**
     * @throws \DomainException
     * @param int $id
     * @return Program
     */
    public function getById(int $id): Program
    {
        /** @var Program $program */
        if(!$program = $this->repository->findOneBy(['id' => $id])){
            throw new \DomainException("Программа не найдена");
        }

        return $program;
    }

    /**
     * @return Program[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param int $id
     */
    public function removeById(int $id): void
    {
        $program = $this->getById($id);

        $this->manager->remove($program);

        return;
    }

    /**
     * @param Program $program
     * @return void
     */
    public function update(Program $program): void
    {
        $this->manager->persist($program);
    }
}
<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Repository;


use App\SimpleDevelopment\Users\Entity\User\User;
use App\SimpleDevelopment\Workout\Entity\Practice\Practice;
use App\SimpleDevelopment\Workout\Entity\Practice\PracticeRepositoryInterface;
use App\SimpleDevelopment\Workout\Entity\Practice\Value;
use App\SimpleDevelopment\Workout\Entity\Program\Program;
use App\SimpleDevelopment\Workout\Entity\Routine\Attribute;
use App\SimpleDevelopment\Workout\Entity\Routine\Routine;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class PracticeRepository
 * @package App\SimpleDevelopment\Workout\Repository
 */
class PracticeRepository implements PracticeRepositoryInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(Practice::class);
    }

    /**
     * @param Practice $practice
     */
    public function add(Practice $practice): void
    {
        $this->em->persist($practice);
    }

    /**
     * @param User $user
     * @return Practice|null
     */
    public function findLast(User $user): ?Practice
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb
                    ->select('p')
                    ->from(Practice::class, 'p')
                    ->where('p.customer = :customer')
                    ->orderBy('p.id', "DESC")
                    ->setMaxResults(1)
                    ->setParameter('customer', $user)
                    ->getQuery();

        try{
            return $query->getOneOrNullResult();
        }catch (NonUniqueResultException $e){
            return end($query->getResult());
        }
    }

    /**
     * @param Program $program
     * @param array $routinesIds
     * @return Value[]
     */
    public function previousValues(Program $program, array $routinesIds): array
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb
                    ->select('v')
                    ->from(Value::class, 'v')
                    ->innerJoin(Program::class, "p")
                    ->innerJoin(Routine::class, 'r')
                    ->where('p.id = :pid')
                    ->andWhere($qb->expr()->in('p.id', $routinesIds))
                    ->setParameter('pid', $program->getId())
                    ->getQuery();

        return $query->getResult();
    }

    /**
     * @param Attribute $attribute
     * @return Value|null
     */
    public function findOneValueByAttribute(Attribute $attribute): ?Value
    {
        $qb = $this->em->createQueryBuilder();

        $query = $qb
            ->select('v')
            ->from(Value::class, 'v')
            ->where('v.attribute = :attribute')
            ->setParameter('attribute', $attribute)
            ->orderBy('v.id', "DESC")
            ->setMaxResults(1)
            ->getQuery();

        try{
            return $query->getOneOrNullResult();
        }catch (NonUniqueResultException $e){
            return end($query->getResult());
        }


    }

    /**
     * @param int $id
     * @return Practice
     */
    public function get(int $id): Practice
    {
        if(!$practice = $this->repository->findOneBy(['id' => $id])){
            throw new \DomainException("Тренировка {$id} не найдена");
        }

        return $practice;
    }

    /**
     * @param Practice $practice
     */
    public function update(Practice $practice): void
    {
        $this->em->persist($practice);
    }
}
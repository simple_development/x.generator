<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Entity\Routine;


use App\SimpleDevelopment\Workout\Entity\Program\Program;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * Class Routine
 * @package App\SimpleDevelopment\Workout\Entity\Routine
 * @ORM\Entity()
 * @ORM\Table(name="workout_routines")
 */
class Routine
{
    /**
     * @Groups({"rest"})
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var int|null
     */
    private $id;

    /**
     * @Groups({"rest"})
     * @ORM\Column(type="string", length=100)
     * @var string
     */
    private $name;

    /**
     * @Groups({"rest"})
     * @ORM\OneToMany(targetEntity="Attribute", mappedBy="routine", cascade={"persist"})
     * @var ArrayCollection|Attribute[]
     */
    private $attributes;

    /**
     * @ORM\ManyToOne(targetEntity="App\SimpleDevelopment\Workout\Entity\Program\Program", inversedBy="routines")
     * @ORM\JoinColumn(name="programs_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Program
     */
    private $program;

    /**
     * Routine constructor.
     * @param string $name
     * @param Program $program
     */
    public function __construct(string $name, Program $program)
    {
        $this->name = $name;
        $this->program = $program;
        $this->attributes = new ArrayCollection();
    }

    /**
     * @param Attribute $value
     */
    public function addAttribute(Attribute $value): void
    {
        if($this->attributes->contains($value)){
            return;
        }

        $this->attributes->add($value);
        $value->setRoutine($this);
        return;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Attribute[]|ArrayCollection
     */
    public function getAttributes(): array
    {
        return $this->attributes->toArray();
    }


}
<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Entity\Routine;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Attribute
 * @package App\SimpleDevelopment\Workout\Entity\Routine
 * @ORM\Entity()
 * @ORM\Table(name="workout_attributes")
 */
class Attribute
{
    /**
     * @Groups({"rest"})
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var int|null
     */
    private $id;

    /**
     * @Groups({"rest"})
     * @ORM\Column(name="value_name",type="string", length=100)
     * @var string
     */
    private $name;

    /**
     * @Groups({"rest"})
     * @ORM\Column(name="value_min",type="integer", length=11)
     * @var int
     */
    private $min;

    /**
     * @Groups({"rest"})
     * @ORM\Column(name="value_max",type="integer", length=11)
     * @var int
     */
    private $max;

    /**
     * @Groups({"rest"})
     * @ORM\Column(name="value_append",type="integer", length=11)
     * @var int
     */
    private $append;

    /**
     * @Groups({"rest"})
     * @ORM\Column(name="value_order", type="integer", length=11)
     * @var int
     */
    private $order;


    /**
     * @ORM\ManyToOne(targetEntity="App\SimpleDevelopment\Workout\Entity\Routine\Routine", inversedBy="values")
     * @ORM\JoinColumn(name="routines_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Routine
     */
    private $routine;


    public function __construct(string $name, int $min, int $max, int $append, int $order)
    {
        $this->name = $name;
        $this->min = $min;
        $this->max = $max;
        $this->append = $append;
        $this->order = $order;
    }


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }



    /**
     * @return Routine
     */
    public function getRoutine(): Routine
    {
        return $this->routine;
    }

    /**
     * @param Routine $routine
     */
    public function setRoutine(Routine $routine): void
    {
        $this->routine = $routine;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getMin(): int
    {
        return $this->min;
    }

    /**
     * @param int $min
     */
    public function setMin(int $min): void
    {
        $this->min = $min;
    }

    /**
     * @return int
     */
    public function getMax(): int
    {
        return $this->max;
    }

    /**
     * @param int $max
     */
    public function setMax(int $max): void
    {
        $this->max = $max;
    }

    /**
     * @return int
     */
    public function getAppend(): int
    {
        return $this->append;
    }

    /**
     * @param int $append
     */
    public function setAppend(int $append): void
    {
        $this->append = $append;
    }


    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order): void
    {
        $this->order = $order;
    }
}
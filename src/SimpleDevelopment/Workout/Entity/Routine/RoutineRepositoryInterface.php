<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Entity\Routine;


use App\SimpleDevelopment\Workout\Entity\Program\Program;

interface RoutineRepositoryInterface
{
    /**
     * @param Routine $routine
     */
    public function add(Routine $routine): void;


    /**
     * @param int $id
     * @param int $limit
     * @param int $offset
     * @return Routine[]
     */
    public function findByProgramId(int $id, int $limit, int $offset): array;

    /**
     * @param Program $program
     * @param int $count
     * @return Routine[]
     */
    public function random(Program $program, int $count): array;
}
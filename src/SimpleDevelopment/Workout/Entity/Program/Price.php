<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Entity\Program;


class Price
{
    /**
     * @var int
     */
    private $price;

    public function __construct(int $price)
    {
        if($price <= 0){
            throw new \DomainException("Стоимость не может быть отрицательной");
        }

        $this->price = $price;
    }


    public function getValue(): int
    {
        return $this->price;
    }
}
<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Entity\Program;


interface ProgramRepositoryInterface
{
    /**
     * @param Program $program
     */
    public function add(Program $program): void;

    /**
     * @throws \DomainException
     * @param int $id
     * @return Program
     */
    public function getById(int $id): Program;

    /**
     * @return Program[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     */
    public function removeById(int $id): void;

    /**
     * @param Program $program
     */
    public function update(Program $program): void;
}
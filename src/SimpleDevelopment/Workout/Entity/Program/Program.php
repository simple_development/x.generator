<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Entity\Program;

use App\SimpleDevelopment\Workout\Entity\Routine\Routine;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * Class Program
 * @package App\SimpleDevelopment\SimpleDevelopment\Entity\Program
 * @ORM\Entity()
 * @ORM\Table(name="workout_programs")
 */
class Program
{

    /**
     * @Groups({"rest"})
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var int|null
     */
    private $id;

    /**
     * @Groups({"rest"})
     * @ORM\Column(type="string", length=30)
     * @var string
     */
    private $title;

    /**
     * @Groups({"rest"})
     * @ORM\Column(type="integer", length=11, options={"unsigned" = true})
     * @var int
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="App\SimpleDevelopment\Workout\Entity\Routine\Routine", mappedBy="program")
     * @var Routine[]|ArrayCollection
     */
    private $routines;

    /**
     * Program constructor.
     * @param Title $title
     * @param Price $price
     */
    public function __construct(Title $title, Price $price)
    {
        $this->changeTitle($title);
        $this->changePrice($price);
        $this->routines = new ArrayCollection();
    }

    /**
     * @param Title $title
     */
    public function changeTitle(Title $title): void
    {
        if($title->getLength() < 3){
            throw new \DomainException("Заголовок не меньше 3 символов");
        }
        $this->title = $title->getValue();
    }

    /**
     * @param Price $price
     */
    public function changePrice(Price $price): void
    {
        $this->price = $price->getValue();
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Routine[]|ArrayCollection
     */
    public function getRoutines()
    {
        return $this->routines;
    }
}
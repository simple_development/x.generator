<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Entity\Program;


class Title
{
    private $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getLength(): int
    {
        return strlen($this->value);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->getValue();
    }
}
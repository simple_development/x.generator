<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Entity\Practice;


use App\SimpleDevelopment\Users\Entity\User\User;
use App\SimpleDevelopment\Workout\Entity\Routine\Routine;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Practice
 * @package App\SimpleDevelopment\Workout\Entity\Practice
 * @ORM\Entity()
 * @ORM\Table(name="workout_practices")
 */
class Practice
{

    /**
     * @Groups({"rest"})
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var int|null
     */
    private $id;


    /**
     * @Groups({"rest"})
     * @ORM\OneToMany(targetEntity="App\SimpleDevelopment\Workout\Entity\Practice\Value",
     *      mappedBy="practice", cascade={"persist"})
     * @var Value[]|ArrayCollection
     */
    private $values;

    /**
     * @Groups({"rest"})
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $success;

    /**
     * @Groups({"rest"})
     * @ORM\Column(type="smallint")
     * @var int
     */
    private $status;

    /**
     * @Groups({"rest"})
     * @ORM\Column(type="date_immutable")
     * @var \DateTimeImmutable
     */
    private $created;

    /**
     * @Groups({"rest"})
     * @ORM\ManyToOne(targetEntity="App\SimpleDevelopment\Users\Entity\User\User")
     * @ORM\JoinColumn(name="users_id", referencedColumnName="id", onDelete="CASCADE")
     * @var User
     */
    private $customer;

    public function __construct(User $customer, \DateTimeImmutable $created)
    {
        $this->values = new ArrayCollection();
        $this->created = $created;
        $this->success = false;
        $this->customer = $customer;
        $this->setStatus(Status::NEW);
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function addValue(Value $value): void
    {
        $this->values->add($value);
        $value->setPractice($this);
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getCreated(): int
    {
        return $this->created->getTimestamp();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }

    /**
     * @return User
     */
    public function getCustomer(): User
    {
        return $this->customer;
    }

    /**
     * @return Value[]|ArrayCollection
     */
    public function getValues(): array
    {

        $result = [];
        /** @var Value $value */
        foreach ($this->values->toArray() as $value){

            if(isset($result[$value->getRoutine()->getId()])){
                $result[$value->getRoutine()->getId()] = array_merge($result[$value->getRoutine()->getId()], [$value]);
            }else{
                $result[$value->getRoutine()->getId()] = [$value];
            }

        }

        return $result;
    }


}
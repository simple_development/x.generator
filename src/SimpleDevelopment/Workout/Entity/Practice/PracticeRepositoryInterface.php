<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Entity\Practice;


use App\SimpleDevelopment\Users\Entity\User\User;
use App\SimpleDevelopment\Workout\Entity\Program\Program;
use App\SimpleDevelopment\Workout\Entity\Routine\Attribute;


interface PracticeRepositoryInterface
{
    /**
     * @param Practice $practice
     */
    public function add(Practice $practice): void;

    /**
     * @param User $user
     * @return Practice|null
     */
    public function findLast(User $user): ?Practice;

    /**
     * @param Program $program
     * @param array $routinesIds
     * @return Value[]
     */
    public function previousValues(Program $program, array $routinesIds): array;

    /**
     * @param Attribute $attribute
     * @return Value|null
     */
    public function findOneValueByAttribute(Attribute $attribute): ?Value;

    /**
     * @param int $id
     * @return Practice
     */
    public function get(int $id): Practice;

    /**
     * @param Practice $practice
     */
    public function update(Practice $practice): void;
}
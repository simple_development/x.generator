<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Entity\Practice;


use App\SimpleDevelopment\Workout\Entity\Routine\Attribute;
use App\SimpleDevelopment\Workout\Entity\Routine\Routine;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Value
 * @package App\SimpleDevelopment\Workout\Entity\Practice
 * @ORM\Entity()
 * @ORM\Table(name="workout_values")
 */
class Value
{
    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var int|null
     */
    private $id;

    /**
     * @Groups({"rest"})
     * @ORM\ManyToOne(targetEntity="App\SimpleDevelopment\Workout\Entity\Routine\Attribute")
     * @ORM\JoinColumn(name="attributes_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Attribute
     */
    private $attribute;

    /**
     * @ORM\ManyToOne(targetEntity="App\SimpleDevelopment\Workout\Entity\Practice\Practice", inversedBy="values")
     * @ORM\JoinColumn(name="practices_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Practice
     */
    private $practice;

    /**
     * @Groups({"rest"})
     * @ORM\ManyToOne(targetEntity="App\SimpleDevelopment\Workout\Entity\Routine\Routine")
     * @ORM\JoinColumn(name="routines_id", referencedColumnName="id", onDelete="SET NULL")
     * @var Routine
     */
    private $routine;
    /**
     * @Groups({"rest"})
     * @ORM\Column(type="integer", length=30)
     * @var int
     */
    private $value;

    public function __construct(Attribute $attribute, Routine $routine, int $value)
    {
        $this->routine = $routine;
        $this->attribute = $attribute;
        $this->setValue($value);
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue(int $value): void
    {
        $this->value = $value;
    }

    /**
     * @return Practice
     */
    public function getPractice(): Practice
    {
        return $this->practice;
    }

    /**
     * @return Attribute
     */
    public function getAttribute(): Attribute
    {
        return $this->attribute;
    }

    /**
     * @param Practice $practice
     */
    public function setPractice(Practice $practice): void
    {
        $this->practice = $practice;
    }

    /**
     * @return Routine
     */
    public function getRoutine(): Routine
    {
        return $this->routine;
    }


}
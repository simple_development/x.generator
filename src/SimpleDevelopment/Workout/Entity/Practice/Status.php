<?php
declare(strict_types=1);

namespace App\SimpleDevelopment\Workout\Entity\Practice;


class Status
{
    public const NEW = 1;
    public const PASSED = 2;
    public const IN_PROCESS = 3;
    public const FAIL = 4;
}
<?php
declare(strict_types=1);

namespace App\DataFixtures;


use App\SimpleDevelopment\Workout\Entity\Practice\Value;
use App\SimpleDevelopment\Workout\Entity\Program\Price;
use App\SimpleDevelopment\Workout\Entity\Program\Program;
use App\SimpleDevelopment\Workout\Entity\Program\Title;
use App\SimpleDevelopment\Workout\Entity\Routine\Routine;
use App\SimpleDevelopment\Workout\Entity\Routine\Attribute;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class StreetFixture extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $program = new Program(new Title("Уличная"), new Price(3000));

        $run = new Routine("Бег", $program);
        $run->addAttribute(new Attribute("Дистанция", 400, 10000, 100, 1));

        $plank = new Routine("Планка", $program);
        $plank->addAttribute(new Attribute("Время", 30, 240, 10, 1));
        $plank->addAttribute(new Attribute("Подходы", 1, 3, 1, 2));



        $manager->persist($program);
        $manager->persist($run);
        $manager->persist($plank);


        $manager->flush();

    }
}
<?php
declare(strict_types=1);

namespace App\Controller\Http\Api\SimpleDevelopment\Workout;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\SimpleDevelopment\Workout\UseCase\Routine\{
    Create,
    Show
};
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * Class RoutineController
 * @package App\Controller\Http\Api\SimpleDevelopment\Workout
 */
class RoutineController extends AbstractController
{

    /**
     * @param Request $request
     * @param Create\Request\Handler $handler
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function create(Request $request, Create\Request\Handler $handler, SerializerInterface $serializer): JsonResponse
    {
        $command = new Create\Request\Command();
        $command->name = $request->get('name');
        $values = json_decode($request->get('values'), true);

        foreach ($values as $value){
            $command->values[] = $serializer->deserialize($value, Create\Request\Value::class, "json");
        }

        $routine = $handler->handle($command);

        return $this->json($routine, 200, [], [
            AbstractNormalizer::GROUPS => ['rest']
        ]);
    }


    public function show(Request $request, Show\Request\Handler $handler): JsonResponse
    {
        $command = new Show\Request\Command();
        $command->programId = (int)$request->query->get('program');

        $routines = $handler->handle($command);

        return $this->json($routines, 200, [], [
            AbstractNormalizer::GROUPS => ['rest']
        ]);
    }
}
<?php
declare(strict_types=1);

namespace App\Controller\Http\Api\SimpleDevelopment\Workout;


use App\Auth\UserFetcher;
use App\Controller\Http\RestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\SimpleDevelopment\Workout\UseCase\Practice\{
    Create,
    Pass,
    Fail
};
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * Class PracticeController
 * @package App\Controller\Http\Api\SimpleDevelopment\Workout
 */
class PracticeController extends RestController
{

    /**
     * @var UserFetcher
     */
    private $userFetcher;

    public function __construct(UserFetcher $userFetcher)
    {
        $this->userFetcher = $userFetcher;
    }

    /**
     * @param Request $request
     * @param Create\Request\Handler $handler
     * @return JsonResponse
     * @throws \Exception
     */
    public function generate(Request $request, Create\Request\Handler $handler): JsonResponse
    {
        $command = new Create\Request\Command();

        $identity = $this->userFetcher->findByToken($request->headers->get('Authorization'));

        $command->userId = (int) $identity->id;
        $command->programId = (int) $request->query->get('program');

        $practice = $handler->handle($command);

        return $this->json($practice, 200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => ['attributes']
        ]);
    }

    /**
     * @param Request $request
     * @param Pass\Request\Handler $handler
     * @return JsonResponse
     */
    public function pass( Request $request, Pass\Request\Handler $handler): JsonResponse
    {
        $command = new Pass\Request\Command();

        $command->practiceId = $request->get('practice');

        $practice = $handler->handle($command);

        return $this->json($practice,200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => ['attributes']
        ]);
    }

    /**
     * @param Request $request
     * @param Fail\Request\Handler $handler
     * @return JsonResponse
     */
    public function fail(Request $request, Fail\Request\Handler $handler): JsonResponse
    {
        $command = new Fail\Request\Command();

        $command->practiceId = $request->get('practice');

        $practice = $handler->handle($command);

        return $this->json($practice,200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => ['attributes']
        ]);
    }

}
<?php
declare(strict_types=1);

namespace App\Controller\Http\Api\SimpleDevelopment\Workout;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\SimpleDevelopment\Workout\UseCase\Program\{
    Create,
    Show,
    Edit,
    Delete
};
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class ProgramController extends AbstractController
{
    /**
     * @return JsonResponse
     */
    public function create(Request $request, Create\Request\Handler $handler): JsonResponse
    {
        $command = new Create\Request\Command();

        $command->title = $request->get('title');
        $command->price = $request->get('price');

        $program = $handler->handle($command);

        return $this->json($program, 200, [], [
            AbstractNormalizer::GROUPS => 'rest'
        ]);
    }


    /**
     * @param Show\Request\Handler $handler
     * @return JsonResponse
     */
    public function getAll(Show\Request\Handler $handler): JsonResponse
    {
        $programs = $handler->handle();

        return $this->json($programs, 200, [], [
            AbstractNormalizer::GROUPS => 'rest'
        ]);
    }


    /**
     * @param int $id
     * @param Delete\Request\Handler $handler
     * @return JsonResponse
     */
    public function delete(int $id, Delete\Request\Handler $handler): JsonResponse
    {
        $command = new Delete\Request\Command();

        $command->programId = $id;

        $handler->handle($command);

        return $this->json([
            'message' => "Категория удалена"
        ]);
    }


    /**
     * @param Request $request
     * @param int $id
     * @param Edit\Request\Handler $handler
     * @return JsonResponse
     */
    public function edit(Request $request, int $id, Edit\Request\Handler $handler): JsonResponse
    {
        $command = new Edit\Request\Command();

        $command->programId = $id;
        $command->title = $request->get('title');
        $command->price = (int) $request->get('price');

        $program = $handler->handle($command);

        return $this->json($program);
    }
}
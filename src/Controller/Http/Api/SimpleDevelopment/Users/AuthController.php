<?php
declare(strict_types=1);

namespace App\Controller\Http\Api\SimpleDevelopment\Users;


use App\Controller\Http\RestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\SimpleDevelopment\Users\UseCase\{
    SignUp,
    LogIn
};

/**
 * Class AuthController
 * @package App\Controller\Http\Api\SimpleDevelopment\Users
 */
class AuthController extends RestController
{

    public function signUp(Request $request, SignUp\Request\Handler $handler): JsonResponse
    {
        $command = new SignUp\Request\Command();

        $command->password = $request->get('password');
        $command->email = $request->get('email');

        $user = $handler->handle($command);

        return $this->json($user);
    }


    public function login(Request $request, LogIn\Request\Handler $handler): JsonResponse
    {
        $command = new LogIn\Request\Command();

        $command->email = $request->get('email');
        $command->password = $request->get('password');

        $user = $handler->handle($command);

        return $this->json($user);
    }



}
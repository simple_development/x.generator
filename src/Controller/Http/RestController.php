<?php
declare(strict_types=1);

namespace App\Controller\Http;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

abstract class RestController extends AbstractController
{
    public function json($data, int $status = 200, array $headers = [], array $context = []): JsonResponse
    {
        $context = array_merge($context, [
            AbstractNormalizer::GROUPS => ['rest']
        ]);


        return parent::json($data, $status, $headers, $context);
    }
}
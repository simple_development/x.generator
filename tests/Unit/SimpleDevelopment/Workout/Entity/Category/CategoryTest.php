<?php
declare(strict_types=1);

namespace App\Tests\Unit\SimpleDevelopment\Workout\Entity\Category;


use App\SimpleDevelopment\Workout\Entity\Category\Attribute;
use App\SimpleDevelopment\Workout\Entity\Category\Category;
use App\SimpleDevelopment\Workout\Entity\Category\Title;
use App\SimpleDevelopment\Workout\Entity\Program\Price;
use App\SimpleDevelopment\Workout\Entity\Program\Program;
use App\SimpleDevelopment\Workout\Entity\Program\Title as ProgramTitle;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    /**
     * @var Program
     */
    private $program;

    protected function setUp(): void
    {
        $this->program = new Program(new ProgramTitle("some"), new Price(300));
    }


    /**
     * @test
     */
    public function create(): void
    {
        $category = new Category(new Title($name = "name"), $this->program);

        parent::assertEquals($name, $category->getTitle());
        parent::assertEquals($this->program, $category->getProgram());
        parent::assertNull($category->getId());
    }

    /**
     * @test
     */
    public function changeTitle(): void
    {
        $category = new Category(new Title($name = "name"), $this->program);

        parent::assertEquals($name, $category->getTitle());

        $title = new Title("some");

        $category->changeTitle($title);

        parent::assertEquals($title->getValue(), $category->getTitle());
    }

    /**
     * @test
     */
    public function addAttributes(): void
    {
        $category = new Category(new Title($name = "name"), $this->program);

        $static = new Attribute("Статическое", $category);
        $run = new Attribute("Беговое", $category);

        $category->addAttribute($static);
        $category->addAttribute($run);

        parent::assertEquals($static, $category->getAttributes()[0]);
        parent::assertEquals($run, $category->getAttributes()[1]);
    }



}
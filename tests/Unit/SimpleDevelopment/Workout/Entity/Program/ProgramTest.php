<?php
declare(strict_types=1);

namespace Tests\Unit\SimpleDevelopment\Workout\Entity\Program;


use App\SimpleDevelopment\Workout\Entity\Program\Price;
use App\SimpleDevelopment\Workout\Entity\Program\Program;
use App\SimpleDevelopment\Workout\Entity\Program\Title;
use PHPUnit\Framework\TestCase;

class ProgramTest extends TestCase
{
    /**
     * @test
     */
    public function create(): void
    {
        $program = new Program(new Title($title = "hello world"), new Price($price = 3000));

        parent::assertEquals($title, $program->getTitle());
        parent::assertEquals($price, $program->getPrice());
        parent::assertIsInt($program->getPrice());
    }

    /**
     * @test
     */
    public function incorrectTitle(): void
    {
        $this->expectExceptionMessage("Заголовок не меньше 3 символов");

        $program = new Program(new Title($title = "1"), new Price($price = 3000));

    }
}
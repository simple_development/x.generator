<?php
declare(strict_types=1);

namespace Tests\Unit\SimpleDevelopment\Workout\Entity\Program;


use App\SimpleDevelopment\Workout\Entity\Program\Price;
use PHPUnit\Framework\TestCase;

class PriceTest extends TestCase
{

    /**
     * @test
     */
    public function create(): void
    {
        $price = new Price($value = 3000);

        parent::assertEquals($value, $price->getValue());
    }


    /**
     * @test
     */
    public function negativeValue(): void
    {
        $this->expectExceptionMessage("Стоимость не может быть отрицательной");

        $price = new Price(-100);
    }

}
<?php
declare(strict_types=1);

namespace App\Tests\Unit\SimpleDevelopment\Workout\Entity\Routine;


use App\SimpleDevelopment\Workout\Entity\Category\Attribute;
use App\SimpleDevelopment\Workout\Entity\Category\Category;
use App\SimpleDevelopment\Workout\Entity\Program\Price;
use App\SimpleDevelopment\Workout\Entity\Program\Program;
use App\SimpleDevelopment\Workout\Entity\Program\Title;
use App\SimpleDevelopment\Workout\Entity\Routine\Routine;
use App\SimpleDevelopment\Workout\Entity\Routine\Attribute;
use PHPUnit\Framework\TestCase;

class RoutineTest extends TestCase
{

    private $category;

    public function setUp(): void
    {
        $this->category = new Category(
            new \App\SimpleDevelopment\Workout\Entity\Category\Title("dwad"),
            new Program(
                new Title("tite"),
                new Price(3000)
            )
        );
    }

    /**
     * @test
     */
    public function create(): void
    {
        $routine = new Routine($name = "dawd");

        parent::assertEquals($name, $routine->getName());
        parent::assertEmpty($routine->getValues());
    }


    /**
     * @test
     */
    public function addValue(): void
    {
        $routine = new Routine($name = "dawd");

        $value = new Attribute(new Attribute("дистанция", $this->category), 100);


        $routine->addValue($value);

        parent::assertEquals($value, $routine->getValues()[0]);
    }
}
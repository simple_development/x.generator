<?php


use App\Controller\Http\Api\SimpleDevelopment\Users\AuthController;
use App\Controller\Http\Api\SimpleDevelopment\Workout\PracticeController;
use App\Controller\Http\Api\SimpleDevelopment\Workout\ProgramController;
use App\Controller\Http\Api\SimpleDevelopment\Workout\RoutineController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes):void {

    $api = $routes->collection('api.')->prefix('/api');


    ### Auth

    $api
        ->add("auth.sign.up", "/auth/signup")
        ->controller([AuthController::class, "signUp"])
        ->methods([Request::METHOD_POST]);


    $api
        ->add("auth.log.in", "/auth/login")
        ->controller([AuthController::class, "login"])
        ->methods([Request::METHOD_POST]);

    ### Practices

    $api
        ->add("practice.generate", "/practices")
        ->controller([PracticeController::class, "generate"])
        ->methods([Request::METHOD_GET]);


    $api
        ->add("practice.pass", "/practices/pass")
        ->controller([PracticeController::class, "pass"])
        ->methods([Request::METHOD_PATCH]);

    $api
        ->add("practice.fail", "/practices/fail")
        ->controller([PracticeController::class, "fail"])
        ->methods([Request::METHOD_PATCH]);


    ### Programs

    $api
        ->add('programs.create', "/programs")
        ->controller([ProgramController::class, "create"])
        ->methods([Request::METHOD_POST]);


    $api
        ->add('programs.get.all', "/programs")
        ->controller([ProgramController::class, "getAll"])
        ->methods([Request::METHOD_GET]);

    $api
        ->add('programs.delete', "/programs/{id}")
        ->controller([ProgramController::class, "delete"])
        ->requirements([
            "id" => "\d+"
        ])
        ->methods([Request::METHOD_DELETE]);

    $api
        ->add('programs.edit', "/programs/{id}")
        ->controller([ProgramController::class, "edit"])
        ->requirements([
            "id" => "\d+"
        ])
        ->methods([Request::METHOD_PUT]);

    ### Routines

    $api
        ->add('routines.create', '/routines')
        ->controller([RoutineController::class, "create"])
        ->methods([Request::METHOD_POST]);

    $api
        ->add('routines.show', "/routines")
        ->controller([RoutineController::class, "show"])
        ->methods([Request::METHOD_GET]);

};